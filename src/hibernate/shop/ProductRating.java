package hibernate.shop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by Lukasz on 14.04.2018.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Double rating;
    String description;
    LocalDateTime createDate;
    @ManyToOne
    User user;
    @ManyToOne
    Product product;
    boolean isVisible;
}
