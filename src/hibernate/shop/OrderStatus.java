package hibernate.shop;

/**
 * Created by Lukasz on 15.04.2018.
 */
public enum OrderStatus {
    NEW, CANCELED, SHIPPED, PENDING, WAITING_FOR_PAYMENT, PAID, DONE
}
