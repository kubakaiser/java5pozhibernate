package hibernate.shop.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Lukasz on 07.04.2018.
 */
// diedziczymy po HttpServlet
public class HelloServlet extends HttpServlet {

    @Override
    //  HttpServletRequest zawiera dane od klienta (przegladarki)
    //  np parametry, dane z formularza, ciastka,
    //  HttpServletResponse zawiera dane ktore odeslemy do klienta (pregladarki)
    //  mozemy wyslac kod strony html, ciastka, zrobic przekierowanie
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    PrintWriter writer = resp.getWriter();
        writer.write("<html><head></head><body><h1>hello 66</h1></body></html>");

    }
}
